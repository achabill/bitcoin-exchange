import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, LoadingController  } from 'ionic-angular';
import { LoginPage } from '../login/login';

import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  public loginCred:any;

  constructor(public nav: NavController, public navParams: NavParams, public alertCtrl: AlertController, public afAuth: AngularFireAuth, public toastCtrl: ToastController, public loadingCtrl: LoadingController) {

    if (this.afAuth.auth.currentUser == null) {

        this.nav.push(LoginPage);
        console.log('No user login'); 

    }

    if(window.localStorage && window.localStorage["crex"]){

        this.loginCred = JSON.parse(window.localStorage["crex"]);

    }else{

          this.nav.push(LoginPage);
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }

  showLoading(c) {
    let loader = this.loadingCtrl.create({
      content: c
    });
    loader.present();
    return loader;
  }

  presentToastWithNoduration(msg) {
  let toast = this.toastCtrl.create({
    message: msg,
    //duration: 5000,
    //dismissOnPageChange: false,
    position: 'bottom',
    showCloseButton: true,
    closeButtonText: "Close"
  });

  toast.onDidDismiss(() => {
    console.log('Dismissed toast');
  });

  toast.present();
  return toast;
}

presentToastWithduration(msg, t) {
  let toast = this.toastCtrl.create({
    message: msg,
    duration: t,
    //dismissOnPageChange: false,
    position: 'bottom',
    showCloseButton: false,
    //closeButtonText: "Close"
  });

  toast.onDidDismiss(() => {
    console.log('Dismissed toast');
  });

  toast.present();
  return toast;
}

validateNumbers(number) {
        var re = /^\d+$/;
          return re.test(number);
       }

validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          return re.test(email);
  }

errorMessageParser(s){

  let errorMsg = "Oops... Something happend. Try again.";

      switch(s){

        case "auth/email-already-in-use":
          errorMsg = "Email already in  use.";
          break;

        case "auth/network-request-failed":
          errorMsg = "Network Error";
          break;

        case "auth/user-not-found":
          errorMsg = "Incorrect Email or Password";
          break;

        case "auth/wrong-password":
          errorMsg = "Incorrect Password";
          break;

        default:
          errorMsg = "Oops... Something happend. Try again.";
          break;
      }

    return errorMsg;

  }


  changePassword() {
    let cpasswordprompt = this.alertCtrl.create({
      title: 'Change Password',
      message: "Please enter your Current Password and New Password to confirm change.",
      inputs: [
        {
          name: 'oldPassword',
          placeholder: 'Current Password',
          type: "password"
        },
        {
          name: 'newPassword',
          placeholder: 'New Password',
          type: "password"
        },{
          name: 'cNewPassword',
          placeholder: 'Confirm New Password',
          type: "password"
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log('Saved clicked');
            let changePasswordLoading = this.showLoading("Changing Password...");
            let credential = firebase.auth.EmailAuthProvider.credential(this.loginCred.email, data.oldPassword);
            this.afAuth.auth.currentUser.reauthenticateWithCredential(credential).then(() => {
                
                if(data.newPassword.length && data.newPassword.length > 7 && data.newPassword == data.cNewPassword){
                   this.afAuth.auth.currentUser.updatePassword(data.newPassword).then(() => {

                        // Update successful.
                        changePasswordLoading.dismiss();
                        this.presentToastWithduration("New Password Saved Successfully", 3000);

                    }).catch((error) => {
                         console.log(error);
                         changePasswordLoading.dismiss();
                         this.presentToastWithduration(this.errorMessageParser(error.code), 3000);
                    });
                  }else{

                      changePasswordLoading.dismiss();
                      this.presentToastWithduration("New Passwords too short or did not match", 3000);
                  }

            }).catch((error) => {
                console.log(error);
                changePasswordLoading.dismiss();
                this.presentToastWithduration(this.errorMessageParser(error.code), 3000);
            });
          }
        }
      ]
    });
    cpasswordprompt.present();
  }

  changeNumber() {
    let cNumberprompt = this.alertCtrl.create({
      title: 'Change Number',
      message: "Please Enter your Current Password and your New Number to confirm change.",
      inputs: [
        {
          name: 'currPassword',
          placeholder: 'Current Password',
          type: "password"
        },
        {
          name: 'newCNum',
          placeholder: '6xx xxx xxx',
          type: "tel"
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            let changePasswordLoading = this.showLoading("Changing Number...");
            let credential = firebase.auth.EmailAuthProvider.credential(this.loginCred.email, data.currPassword);
            this.afAuth.auth.currentUser.reauthenticateWithCredential(credential).then(() => {
                
                if(data.newCNum && data.newCNum.length == 9 && this.validateNumbers(data.newCNum) && data.newCNum.substr(0,1) == 6){
                   this.afAuth.auth.currentUser.updateProfile({displayName: data.newCNum, photoURL: ""}).then(() => {

                        // Update successful.
                        changePasswordLoading.dismiss();
                        this.loginCred.phone = data.newCNum;
                        window.localStorage["crex"] = JSON.stringify(this.loginCred);
                        this.presentToastWithduration("New Number Saved Successfully", 3000);

                    }).catch((error) => {
                         console.log(error);
                         changePasswordLoading.dismiss();
                         this.presentToastWithduration(this.errorMessageParser(error.code), 3000);
                    });
                  }else{

                      changePasswordLoading.dismiss();
                      this.presentToastWithduration("Invalid Phone Number", 3000);
                  }

            }).catch((error) => {
                console.log(error);
                changePasswordLoading.dismiss();
                this.presentToastWithduration(this.errorMessageParser(error.code), 3000);
            });
          }
        }
      ]
    });
    cNumberprompt.present();
  }



  changeEmail() {
    let cNumberprompt = this.alertCtrl.create({
      title: 'Change Email',
      message: "Please Enter your Current Password and New Email to confirm change.",
      inputs: [
        {
          name: 'currPassword',
          placeholder: 'Current Password',
          type: "password"
        },{
          name: 'newEmail',
          type: 'email',
          placeholder: 'newemail@email.com'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log('Saved clicked');
            let changePasswordLoading = this.showLoading("Changing Email...");
            let credential = firebase.auth.EmailAuthProvider.credential(this.loginCred.email, data.currPassword);
            this.afAuth.auth.currentUser.reauthenticateWithCredential(credential).then(() => {
                
                if(data.newEmail && this.validateEmail(data.newEmail)){
                   this.afAuth.auth.currentUser.updateEmail(data.newEmail).then(() => {

                        // Update successful.
                        changePasswordLoading.dismiss();
                        this.loginCred.email = data.newEmail;
                        window.localStorage["crex"] = JSON.stringify(this.loginCred);
                        this.presentToastWithduration("New Email Saved Successfully", 3000);

                    }).catch((error) => {
                         console.log(error);
                         changePasswordLoading.dismiss();
                         this.presentToastWithduration(this.errorMessageParser(error.code), 3000);
                    });
                  }else{

                      changePasswordLoading.dismiss();
                      this.presentToastWithduration("Invalid Email", 3000);
                  }

            }).catch((error) => {
                console.log(error);
                changePasswordLoading.dismiss();
                this.presentToastWithduration(this.errorMessageParser(error.code), 3000);
            });
          }
        }
      ]
    });
    cNumberprompt.present();
  }


  setDefaultSetting() {
    let setDefaultSettingConfirm = this.alertCtrl.create({
      title: 'Restore Default Settings',
      message: 'Are you sure you want to restore default settings?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            console.log('Agree clicked');
          }
        }
      ]
    });
    setDefaultSettingConfirm.present();
  }


  showAppAlert(t,s) {
    let appAlert = this.alertCtrl.create({
      title: t,
      subTitle: 'Please visit our site <b>www.crytoexchange.com</b> for more information <b>'+s+"</b>",
      buttons: ['OK']
    });
    appAlert.present();
  }

  changeEmailNoti() {
      
      this.loginCred.emailNoti = !this.loginCred.emailNoti;
      window.localStorage["crex"] = JSON.stringify(this.loginCred);
  }


  changeTextNoti() {
   
      this.loginCred.textNoti = !this.loginCred.textNoti;
      window.localStorage["crex"] = JSON.stringify(this.loginCred);
  }


  logout(){

        if(window.localStorage && window.localStorage["crex"]){

        let changePasswordLoading = this.showLoading("Logging you out...");
        window.localStorage["crex"] = "";
        //this.afAuth.auth.logout();
        //this.afAuth.getInstance().signOut();
       
        this.afAuth.auth.signOut().then(() => {

            changePasswordLoading.dismiss();
            this.nav.push(LoginPage);

        }).catch((error) => {

            changePasswordLoading.dismiss();
            console.log(error);
            this.presentToastWithduration(this.errorMessageParser(error.code), 3000);

        });

        }

  }

}
