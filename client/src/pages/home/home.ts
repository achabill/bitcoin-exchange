import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SettingsPage } from '../settings/settings';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

	public buyData;
	public rates;

  constructor(public nav: NavController) {

  	this.buyData = {

  		cfa: null,
  		usd: null,
  		btc: null,
  		fee: null,
  		address: "",
  		momo: "m",
  		tel: ""
  	}

  	this.rates = {

  		usdRate: 550,
  		btcRate: 6950,
  		tfee: 10,
  	}
  }

  goToSettings(){
    
     this.nav.push(SettingsPage);

  }

  computeValuesFromCFA(val){

  	//this.buyData.cfa = this.buyData.usd * this.rates.usdRate;
  	this.buyData.usd = (val / this.rates.usdRate).toFixed(2);
  	this.buyData.btc = (this.buyData.usd / this.rates.btcRate).toFixed(8);
  	this.buyData.fee = (this.buyData.btc * (this.rates.tfee / 100)).toFixed(8);
  }

  computeValuesFromUSD(val){

  	this.buyData.cfa = (this.buyData.usd * this.rates.usdRate).toFixed(0);
  	this.buyData.btc = (val / this.rates.btcRate).toFixed(8);
  	this.buyData.fee = (this.buyData.btc * (this.rates.tfee / 100)).toFixed(8);
  	this.buyData.usd = (val * 1.0).toFixed(2);
  }

  computeValuesFromBTC(val){

  	this.buyData.usd = (val * this.rates.btcRate).toFixed(2);
  	this.buyData.cfa = this.buyData.usd * this.rates.usdRate;
  	this.buyData.fee = (val * (this.rates.tfee / 100)).toFixed(8);
  	this.buyData.btc = (val * 1.0).toFixed(8);
  }

}
