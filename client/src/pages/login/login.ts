import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { SignupPage } from '../signup/signup';
import { TabsPage } from '../tabs/tabs';

import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

import { Http, Headers, RequestOptions } from '@angular/http';

import { Util } from '../util/util'

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

	public showLoginForm: boolean;
  public loginData;
  public forgotPassData;
  public testDb: AngularFireList<any>;
  public loginCred;

  constructor(public nav: NavController, public navParams: NavParams, public alertCtrl: AlertController, public afDatabase: AngularFireDatabase, public afAuth: AngularFireAuth, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public http: Http) {

  	this.showLoginForm = false;
    this.loginData = {

      email: null,
      emailValColor: "primary",
      password: null,
      passwordValColor: "primary",
      logInError: " "

    }

    if (this.afAuth.auth.currentUser != null) {

        this.nav.push(TabsPage);
        console.log(this.afAuth.auth.currentUser);

    }

    if(window.localStorage && window.localStorage["crex"]){

        this.loginCred = JSON.parse(window.localStorage["crex"]);
        if(this.loginCred.email && this.loginCred.password){

          this.loginData.email = this.loginCred.email;
          this.loginData.password = this.loginCred.password;
          this.login();

        }

    }else{

          this.loginCred = {
         
            email: "",
            phone: "",
            password: "",
            unitBTC: "BTC",
            unitInt: "USD",
            unitLoc: "FCFA",
            emailNoti: true,
            textNoti: true
          };
        //window.localStorage["crex"] = JSON.stringify(this.loginCred);
    }





    this.forgotPassData = {

      email: null

    }

    /*this.testDb = afDatabase.object('/app').valueChanges().subscribe((data) => {
    
    console.log(data);
    
      this.testDb.unsubscribe();
    }, (err) => {

          console.log(err.message, err.code);

      }); */
      let util = new Util();
      //let headers = new Headers({ 'Access-Control-Allow-Origin': '*' });
      //let options = new RequestOptions({ headers: headers });
      this.getVars = this.http.get("http://"+util.serverHost+":"+util.serverPort+"/cryptoexchange/api/v1/variables").subscribe((data) => {
    
          console.log(data);
          this.getVars.unsubscribe();

    }, (err) => {

          console.log(err.message, err.code, util.serverHost+":"+util.serverPort+"/cryptoexchange/api/v1/variables");

      }); 
    

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  goToHome(){

  		this.nav.push(TabsPage);
  }

  goToSignUp(){

  		this.nav.push(SignupPage);
  }

  presentToastWithduration(msg, t) {
  let toast = this.toastCtrl.create({
    message: msg,
    duration: t,
    //dismissOnPageChange: false,
    position: 'bottom',
    showCloseButton: false,
    //closeButtonText: "Close"
  });

  toast.onDidDismiss(() => {
    console.log('Dismissed toast');
  });

  toast.present();
  return toast;
}

  errorMessageParser(s){

      switch(s){

        case "auth/email-already-in-use":
          this.loginData.logInError = "Email already in  use.";
          break;

        case "auth/network-request-failed":
          this.loginData.logInError = "Network Error";
          this.presentToastWithduration("Oops... Can't connect to the Internet. Check your internet connection", 5000);
          break;

        case "auth/user-not-found":
          this.loginData.logInError = "Incorrect Email or Password";
          break;

        case "auth/wrong-password":
          this.loginData.logInError = "Incorrect Password";
          break;

        default:
          this.loginData.logInError = "Oops... Something happend.";
          break;
      }

  }

  forgotPassDialog() {
    let forgotPassprompt = this.alertCtrl.create({
      title: 'Forgot Password',
      message: "Please Enter your email and we will send you an email with steps on how to reset your password.",
      inputs: [
        {
          name: 'newEmail',
          type: 'email',
          placeholder: 'example@email.com'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked', data);
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log('Saved clicked',data);
          }
        }
      ]
    });
    forgotPassprompt.present();
  }


  showAlert(t, sT) {
    let alert = this.alertCtrl.create({
      title: t,
      subTitle: sT,
      buttons: ['OK']
    });
    alert.present();
    return alert;
  }

  showLoading() {
    let loader = this.loadingCtrl.create({
      content: "Logging in..."
    });
    loader.present();
    return loader;
  }


  validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          return re.test(email);
  }

  validateNumbers(number) {
        var re = /^\d+$/;
          return re.test(number);
       }


  genPassword(length,s){

      var text = "";
      var possible = "";
      var sampleSpaceInt = "0123456789";
      var sampleSpaceChar = "abcdefghijklmnopqrstuvwxyz";
      var sampleSpaceCharCaps = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
      var sampleSpace = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      if(s == 'A'){

        possible = sampleSpaceCharCaps;
      }else if(s == 'a'){

        possible = sampleSpaceChar;
      }else if(s == '0'){

        possible = sampleSpaceInt;
      }else{

        possible = sampleSpace;
      }

      for(var i = 0; i < length; i++) {
            text += possible. charAt(Math. floor(Math. random() * possible. length));

        }
            return text;
  }


  validateLoginForm(){

      if (this.loginData.email && this.validateEmail(this.loginData.email)) {
         if (this.loginData.password) {

            return true;

         }else{

            this.loginData.passwordValColor = "danger";
            return false;

         }
      }else{

          this.loginData.emailValColor = "danger";
          return false;
      }
  }


  login(){

      if (this.validateLoginForm()) {

          let loginLoader = this.showLoading();
          console.log("vall conrect");
          //this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
          this.afAuth.auth.signInWithEmailAndPassword(this.loginData.email, this.loginData.password).then((res) => {
            
              console.log(res);
              
                this.loginCred.email = this.loginData.email,
                this.loginCred.phone =  res.displayName,
                this.loginCred.password =  this.loginData.password,

              window.localStorage["crex"] = JSON.stringify(this.loginCred);
              loginLoader.dismiss();
              this.nav.push(TabsPage);

          }).catch((err) => {
        
              console.log(err);
              loginLoader.dismiss();
              this.errorMessageParser(err.code);

          });
      }
  }

}
