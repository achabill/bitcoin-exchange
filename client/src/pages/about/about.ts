import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TransactionPage } from '../transaction/transaction';
import { SettingsPage } from '../settings/settings';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  constructor(public nav: NavController) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad loaded');
  }

  viewTransaction(){

     this.nav.push(TransactionPage);

  }

  goToSettings(){
    
     this.nav.push(SettingsPage);

  }

}
