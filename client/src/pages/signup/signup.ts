import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { TabsPage } from '../tabs/tabs';

import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

	public showSignupForm: boolean;
  public signupData;
  public loginCred;

  constructor(public nav: NavController, public navParams: NavParams, public afAuth: AngularFireAuth, public loadingCtrl: LoadingController) {

  	this.showSignupForm = false;
    this.signupData = {

        email: null,
        emailValColor: "primary",
        phone: null,
        phoneValcolor: "primary",
        password: null,
        cPassword: null,
        passwordValColor: "primary",
        signupError: " "
    }


    if (this.afAuth.auth.currentUser != null) {

        this.nav.push(TabsPage);

    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  goToHome(){

  		this.nav.push(TabsPage);
  }

  goToLogin(){

  		this.nav.push(LoginPage);
  }


   validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          return re.test(email);
  }

  validateNumbers(number) {
        var re = /^\d+$/;
          return re.test(number);
       }

showLoading() {
    let loader = this.loadingCtrl.create({
      content: "Logging in..."
    });
    loader.present();
    return loader;
  }

  errorMessageParser(s){

      switch(s){

        case "auth/email-already-in-use":
          this.signupData.signupError = "Email already in use.";
          break;

        case "auth/network-request-failed":
          this.signupData.signupError = "Network Error";
          break;

        default:
          this.signupData.signupError = "Oops... Something happend.";
          break;
      }

  }


  validateSignupForm(){

       if (this.signupData.email && this.validateEmail(this.signupData.email)) {
        if (this.signupData.phone && this.validateNumbers(this.signupData.phone) && this.signupData.phone.length == 9 && this.signupData.phone.substr(0,1) == 6) {
          if (this.signupData.password && this.signupData.cPassword && this.signupData.password.length > 7 && this.signupData.password == this.signupData.cPassword) {

              return true;

          }else{

              this.signupData.passwordValColor = "danger";
              this.signupData.signupError = "Passwords did not match or too short, Min is 8 characters"
              return false;

          }

        }else{

            this.signupData.phoneValcolor = "danger";
            return false;
        }

       }else{

        this.signupData.emailValColor = "danger";
        return false;
       }
  }



  signup(){

      if (this.validateSignupForm()) {

         let signupLoader = this.showLoading();
          console.log("vall conrect");
          //this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
          this.afAuth.auth.createUserWithEmailAndPassword(this.signupData.email, this.signupData.password).then((res) => {
            
              console.log(res);
              let newUser = this.afAuth.auth.currentUser.updateProfile({displayName: this.signupData.phone, photoURL: ""}).then((res) => {

                    console.log(res);
                    this.loginCred = {
              
                      email: this.signupData.email,
                      phone: this.signupData.phone,
                      password: this.signupData.password,
                      unitBTC: "BTC",
                      unitInt: "USD",
                      unitLoc: "FCFA",
                      emailNoti: true,
                      textNoti: true


                    };

                    window.localStorage["crex"] = JSON.stringify(this.loginCred);
                   signupLoader.dismiss();

                }).catch((err) => {

                  console.log(err);
                   signupLoader.dismiss();

                });

          }).catch((err) => {
        
              console.log(err);
              signupLoader.dismiss();
              this.errorMessageParser(err.code);

          });
      }
  }

}
