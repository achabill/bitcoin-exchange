'use strict';

function SellRequest(amount, date_created, to_address, phone_number, money_platform, charge, uid, status) {
    this.amount = amount;
    this.date_created = date_created;
    this.to_address = to_address;
    this.charge = charge;
    this.uid = uid;
    this.phone_number = phone_number;
    this.money_platform = money_platform;
    this.status = status;
}

module.exports = SellRequest;