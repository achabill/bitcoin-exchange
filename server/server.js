var express = require('express');
var app = express();
var bodyParser = require('body-parser');

var admin = require('firebase-admin');
var serviceAccount = require("./cryptoexchange-7ac3f-firebase-adminsdk-57xim-3a5f0a5991.json");
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://cryptoexchange-7ac3f.firebaseio.com"
});

var variables = require('./env/variables')

//configure app to use bodyparaser to get POST data from requests
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Add headers
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8100');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

var port = process.env.PORT || 8080;

//resources
var buyResource = require('./resources/buyResource');
var currencyConverterResource = require('./resources/currencyConverterResource');
var sellResource = require('./resources/sellResource');
var variablesResource = require('./resources/variablesResource');

//add routers
app.use(variables.base_api, variablesResource.router());
app.use(variables.base_api, buyResource.router());
app.use(variables.base_api, currencyConverterResource.router());
app.use(variables.base_api, sellResource.router());


//start the server
app.listen(port);
console.log('Listening on port: ' + port);
if (variables.dev_mode) {
    console.log("DEV MODE");
}