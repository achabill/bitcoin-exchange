'use strict'
var admin = require('firebase-admin');


var sellRequestDao = {
    db: null,
    ref: null,
    initializeDao: function () {
        var self = this;
        self.db = admin.database();
        self.ref = self.db.ref().child("sellRequests");
    },
    add: function (request) {
        var self = this;
        return Promise.resolve(self.ref.push(request));
    },
    update: function (key, newRequest) {
        var self = this;
        return Promise.resolve(self.ref.child(key).update(newRequest));
    },
    delete: function (key) {
        var self = this;
        return Promise.resolve(self.ref.child(key).set(null));
    },
    get: function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.ref.on("value", function (snapshot) {
                resolve(snapshot.val());
            }, function (errorObject) {
                reject("The read failed: " + errorObject.code);
            });
        });
    },
    getOne: function (key) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.ref.child(key).on("value", function (snapshot) {
                resolve(snapshot.val());
            }, function (errorObject) {
                reject("The read failed: " + errorObject.code);
            });
        });
    }
}

sellRequestDao.initializeDao();
module.exports = sellRequestDao;