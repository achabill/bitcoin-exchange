'use strict';

/*N.B 1 satoshi == .000000001 BTC */
/* ALL MONETY UNITS ARE IN satoshi */

var variables = {
    from_address: null,                     //wallet address to send coins from
    blockchain_guid: 'd54bf466-4d72-455f-94b8-72f35d33961d',                  //blockchain guid.
    blockchain_main_password: null,         //blockchain.com main password
    blockchain_second_password: null,       //blockchain.com seconday password. If set
    base_api: '/cryptoexchange/api/v1',     //base api
    blockchain_base_path: 'https://blockchain.info/merchant',
    blockchain_fee_btc: 0.0001,             //BTC.
    futhermarket_accountId: null,           //futhermarket business account id
    futhermarket_orange_number: null,       //futhermarket bussiness account orange number
    futhermarket_momo_number: null,         //futhermarket business account momo number
    futhermarket_orange_number: null,       //futhermarket business account orange number
    futhermarket_cost_percentage: 1,        //futhermarket collects 1% of the amount being received or sent
    futhermarket_momo_request_payment_base: 'http://api.furthermarket.com/FM/MTN/MoMo/',
    buy_transaction_charge_percentage: 7,   //percentage of amount to charge for buy transaction
    sell_transaction_charge_percentage: 9,  //percentage of amount to charege for sell transaction
    min_amount: 0.00645,                    //BTC === 50 USD
    auto_process: true,                      //if the request submitted to request processor should be processed
    currencylayer_access_key: 'afe8aaa70e1ef7b8a10e52085634a399', //access key to https://currencylayer.com/
    currencylayer_api_base: 'http://apilayer.net/api/', //currency layer api base
    dollars: 'USD',
    francs: 'XAF',
    bitcoin: 'BTC',
    blockchain_xpub: 'xpub6CxShHPs7THuc7rT1MjhNhwUChGLT6AvgckSJfeAg86CnJZF7r8k9Yew3skNnEp9JXqq6U61HTik4J87kD9m72swygbGdVEGrYXvBRmCHUx',
    blockchain_api_key: '',
    blockchain_receive_callback_url: 'https://crptoexchange.come/received',
    mobile_money_platform_code: 'MM',
    orange_money_platform_code: 'OM',
    selling_mobile_money_actived: false,
    buying_mobile_money_actived: true,
    selling_orange_money_activated: false,
    buying_orange_money_activated: false,
    dev_mode: true
};
module.exports = variables;