# Crypto Exchange

## Models
 - BuyRequest
    A request sent to the server to buy coins

    ```
    amount : float,
    date_created : date,
    to_address = string,
    from_address = string,
    charge = float,
    uid = string,
    phone_number = int,
    money_platform = string [ 'OM','MM']
    status = string ['pending','processing','complete','rejected']
    ```
 - Sell request
    A request sent to the server to sell coins
    ```
    amount = float,
    date_created = date,
    to_address = string,
    charge = float,
    uid = string,
    phone_number = string,
    money_platform =  string [ 'OM','MM']
    status = string ['pending','processing','complete','rejected']
    ```

 - Charge  
    The charge on a transaction
    ```
    xaf: float, The charge in cfa
    btc: float  The charge in btc
    ```
    
## API

Base url for all endpoints is defined as `base_api` in variables.js  

| endpoint   |      description      |  body | params | query |  result |
|:----------|:-------------|:------|:------- |:------ |:------|
| `base_api`/convert |  converts an amount from one currenty to another | null | null | `from, to, amount, token` | the converted amount |
| `base_api`/currencies | returns exchange rates relative to USD | null | null | `currencies, token` | the quotes |
| `base_api`/buy | buy bitcoints with cash | `{amount, to_address, phone_number, money_platform}`| null | `token` | A buy request |
| `base_api`/sell | sell bitcoins and get cash | `{amount, to_address, phone_number, money_platform`} |  null | `token` | A sell request
| `base_api`/buy/charge | Get the charge on a buy amount in btc | null | null | `amount, token` | `{xaf, btc}` |
| `base_api`/sell/charge | Get the charge on a sell amount in btc | null | null | `amount,token` | `{xaf, btc}` |
| `base_api/blockchain_receive_callback_url` | callback when blockchain revieves coins | null | null | key  |  completed sell requst |
| `base_api/variables` | Get variables used on the server | null | null | `token` | variables object |

## Sample

 - To get currency exchange rates relative to USD
    GET: `base_api/currencies?token=your_token&currencies=BTC,XAF`

    ```
    {
        "USDBTC": 0.000121,
        "USDXAF": 556.77002
    }
    ```
 - To convert amount from one currency to another
    GET: `base_api/convert?token=your_token&amount=10&from=USD&to=GBP`

    ```
    6.58443
    ```

