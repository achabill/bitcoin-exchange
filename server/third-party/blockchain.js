'use strict';

//var jsonqueryString = require('json-query-string');
var queryString = require('querystring');
var request = require('request');
var variables = require('../env/variables');
var blockchain = {
    send: function (_to, _amount) {
        var self = this;

        return new Promise(function (resolve, reject) {
            if (variables.dev_mode) {
                resolve({
                    message: 'Sent ' + _amount + 'BTC to ' + _to,
                    tx_hash: 'f322d01ad784e5deeb25464a5781c3b20971c1863679ca506e702e3e33c18e9c',

                });
            } else {
                var obj = {
                    fee: self.variables.fee,
                    main_password: self.variables.blockchain_main_password,
                    to: _to,
                    amount: _amount,
                    fee: self.variables.fee
                };

                //prepaire uri
                var uri = variables.blockchain_base_path + '/' + variables.blockchain_guid + '/payment?' +
                    queryString.stringify(obj);

                //call api
                self.request.get(uri, function (res) {
                    resolve(res);
                }, function (error) {
                    reject(error);
                });

            }
        });
    },

    receive: function (key) {
        var self = this;
        return new Promise(function (resolve, reject) {
            var obj = {
                xpub: variables.blockchain_xpub,
                callback_url: escape(variables.blockchain_receive_callback_url + '/?key=' + key),
                key: variables.blockchain_api_key
            };
            url = 'https://api.blockchain.info/v2/receive?' + queryString.stringify(obj);
            request.get(url, function (res) {
                resolve(res.address);
            }, function (err) {
                reject(err);
            });
        });
    }
};

module.exports = blockchain;