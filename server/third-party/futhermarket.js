'use strict'
//var jsonqueryString = require('json-query-string');
var queryString = require('querystring');
var request = require('request');
var variables = require('../env/variables');
var currencyLayer = require('../third-party/currencylayer');

var futherMarket = {
    receive: function (request) {
        var self = this;
        if (request.mobile_platform == 'MM') {
            var amount = request.charge.btc + request.charge.amount;
            currencyLayer.convert(variables.bitcoin, variables.francs, amount).then(function (result) {
                return self.receive_momo(request.phone_number, result);
            });
        } else if (request.mobile_platform == 'OM') {
            return self.receive_orange();
        } else {
            return new Promise(function (resolve, reject) {
                reject('unknown mobile platform: ' + request.money_platform + 'use {OM or MM}');
            });
        }
    },
    receive_momo: function (client_momo_number, amount) {
        return new Promise(function (resolve, reject) {
            if (variables.dev_mode) {
                //sleep for some time between 30 and 100 seconds to simulate mobile money payment.
                var deasync = require('./deasync');
                var high = 100;
                var low = 30;
                var duraction = Math.random() * (high - low) + low;
                var sleep = deasync(function (timeout, done) {
                    setTimeout(done, timeout);
                });
                resolve({
                    status: 1,
                    paymentId: '88',
                    comments: 'Successful Payment. Successfully processed transaction'
                });
            } else {
                var obj = {
                    MyaccountId: variables.futhermarket_accountId,
                    CustomerPhoneNumber: client_momo_number,
                    Amount: amount
                };
                var url = variables.futhermarket_momo_request_payment_base + 'requestpayment/?' + queryString.stringify(obj);
                request.post(url, function (res) {
                    resolve(res);
                }, function (err) {
                    reject(err);
                });
            }
        });
    },
    receive_orange: function () { },
    check_receive: function (paymentid, amount) {
        return new Promise(function (resolve, reject) {
            if (variables.dev_mode) {
                //dummy check
                resolve({
                    status: 1,
                    paymendId: 88,
                    amount: amount,
                    currency: 'XAF',
                    payerAccountId: '11125364',
                    payerPhoneNumber: '679873401',
                    receiverAccountId: '88974562',
                    timeStamp: '111254365',
                    operator: 'MTN Mobile Money'
                });
            } else {
                var obj = {
                    accountId: variables.futhermarket_accountId,
                    paymentId: paymentid
                };
                var url = variables.futhermarket_momo_request_payment_base + 'checkpayment/?' + queryString.stringify(obj);
                request.post(url, function (res) {
                    resolve(res);
                }, function (err) {
                    reject(err);
                });
            }

        });
    },
    send: function (request) {
        return new Promise(function (resolve, reject) {
            resolve('dummy send');
        })
    },
    send_momo: function (phone_number, amount) {

    },
    send_orange: function () { },
    check_send: function (paymentId) {
    }
};

module.exports = futherMarket;