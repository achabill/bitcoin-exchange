'use strict';

var variables = require('../env/variables');
//var jsonqueryString = require('json-query-string');
var queryString = require('querystring');
var request = require('request');

var currencyLayer = {
    convert: function (from, to, amount) {
        var self = this;

        return new Promise(function (resolve, reject) {
            self.currencies('' + from + ',' + to).then(function (res) {
                var result = res['quotes']['USD' + from] / res['quotes']['USD' + to];
                resolve(result);
            }, function (err) {
                reject(err);
            })
        });
    },
    //returns the value of currencies relatives to USD
    currencies: function (currencies) {
        var obj = {
            access_key: variables.currencylayer_access_key,
            currencies: currencies
        };
        return new Promise(function (resolve, reject) {
            var url = variables.currencylayer_api_base + '/live?' + queryString.stringify(obj);
            console.log(url);
            request.get(url, function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            }).catch(function (err) {
                reject(err);
            });
        })
    }
};

module.exports = currencyLayer;