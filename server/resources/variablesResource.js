'use strict';

/**
 * Variables resource
 */

var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var admin = require('firebase-admin');
var variables = require('../env/variables');
var currencyLayer = require('../third-party/currencylayer');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var variablesResource = {
    router: function () {
        var r = express.Router();
        var self = this;
        r.route('/variables')
            .get(function (req, res) {
                var token = req.query.token;
                admin.auth().verifyIdToken(token).then(function (decodedToken) {
                    var r = {
                        buy_transaction_charge_percentage: variables.buy_transaction_charge_percentage,
                        sell_transaction_charge_percentage: variables.sell_transaction_charge_percentage,
                        blockchain_fee_btc: variables.blockchain_fee_btc,
                        min_amount: variables.min_amount,
                        auto_process: variables.auto_process,
                        dollars: variables.dollars,
                        francs: variables.francs,
                        bitcoin: variables.bitcoin,
                        mobile_money_platform_code: variables.mobile_money_platform_code,
                        ornage_money_platform_code: variables.orange_money_platform_code,
                        selling_mobile_money_actived: variables.selling_mobile_money_actived,
                        buying_mobile_money_actived: variables.buying_mobile_money_actived,
                        selling_orange_money_activated: variables.selling_orange_money_activated,
                        buying_orange_money_activated: variables.buying_orange_money_activated,
                        dev_mode: variables.dev_mode,
                        currencies: null
                    };
                    currencyLayer.currencies('BTC,XAF').then(function (res) {
                        r.currencies = res.quotes;
                        res.json(r);
                    }, function (err) {
                        res.json(err);
                    });

                    res.json(variables);
                }).catch(function (err) {
                    res.status(504).json({
                        error: err
                    });
                });
            });
        return r;
    }
};

module.exports = variablesResource;