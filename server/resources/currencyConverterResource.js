'use strict';

var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var currencyLayer = require('../third-party/currencylayer');
var admin = require('firebase-admin');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var currencyConverterResource = {
    router: function () {
        var r = express.Router();
        var self = this;
        r.route('/convert').get(function (req, res) {
            var from = req.query.from;
            var to = req.query.to;
            var amount = req.query.amount;
            var token = req.query.token;

            admin.auth().verifyIdToken(token).then(function (decodedToken) {
                currencyLayer.convert(from, to, amount).then(function (result) {
                    res.json(result);
                }, function (err) {
                    res.status(404).json({
                        error: err
                    });
                }).catch(function (c) {
                    res.json(c);
                });
            }).catch(function (err) {
                res.status(504).json({
                    error: err
                });
            });


        });
        r.route('/currencies').get(function (req, res) {
            var currencies = req.query.currencies;
            var token = req.query.token;
            admin.auth().verifyIdToken(token).then(function (decodedToken) {
                currencyLayer.currencies(currencies).then(function (res) {
                    res.json(res.quotes);
                }, function (err) {
                    res.status(504).json({
                        error: err
                    });
                });

            }).catch(function (err) {
                res.status(504).json({
                    error: err
                });
            });;
        })
        return r;
    }
}

module.exports = currencyConverterResource;