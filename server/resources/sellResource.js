'use strict';

/**
 * Sell resource
 */

var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var admin = require('firebase-admin');
var requestCharge = require('../requests/requestCharge');
var futherMarket = require('../third-party/futhermarket');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var variables = require('../env/variables');

/**
 * Sell resource
 */
var sellResource = {
    sellRequestQueue: require('../requests/sellRequestQueue'),
    sellRequestDao: require('../dao/sellRequestDao'),
    /**
     * buy request router
     */
    router: function () {
        var r = express.Router();
        var self = this;
        r.route('/sell')
            /**
             * Adds a new sellrequest to the request queue.
             */
            .post(function (req, res) {

                //firebase authentication idtoken.
                var token = req.query.token;

                var SellRequest = require('../models/sellRequest');

                //verify idtoken and proceed to add sellRequest
                admin.auth().verifyIdToken(token)
                    .then(function (decodedToken) {
                        console.log("token verified");
                        //decode uid
                        var uid = decodedToken.uid;

                        //create sellRequest object
                        var sellRequest = new SellRequest(req.body.amount, Date.now(), req.body.to_address,
                            req.body.phone_number, req.body.money_platform, requestCharge.sellCharge(amount), uid, 'pending');

                        //add buy request to the sellRequest queue.
                        self.sellRequestQueue.enqueue(sellRequest).then(function (request) {
                            res.json(request);
                        }, function (error) {
                            res.status(404).json({
                                error: error
                            });
                        })
                    }).catch(function (error) {
                        res.status(404).json({
                            error: error
                        });
                    });
            });
        r.route('/sell/charge').get(function (req, res) {
            var token = req.query.token;
            admin.auth().verifyIdToken(token).then(function (decodedToken) {
                var amount = req.query.amount;
                if (amount == undefined) {
                    res.status(404).json({
                        error: 'amount is undefined'
                    });
                } else {
                    requestCharge.sellCharge(amount).then(function (result) {
                        res.json(result);
                    }, function (err) {
                        res.status(404).json({
                            error: err
                        });
                    });
                }
            }).catch(function (err) {
                res.status(504).json({
                    error: err
                });
            });

        });
        //when blockchain revies bitcoin
        r.route(variables.blockchain_receive_callback_url).get(function (req, res) {
            var key = req.query.key;
            self.sellRequestDao.getOne(key).then(function (request) {
                //set the request as 'pending payment' and wait for manual processing by mobile money
                request.status = 'pending payment';
                self.sellRequestDao.update(key, request).then(function (res) {
                    res.json(request);
                },
                    function (err) {
                        res.json(err);
                    });

                /* futherMarket.send(request).then(function (result) {
                 request.status = 'complete';
                 self.sellRequestDao.update(key, request).then(function (result) {
                     res.json(request);
                 }, function (err) {
                     res.json(err);
                 })
             }, function (err) {
                 res.json(err);
             });
             */
            });
        });
        return r;
    }
};
module.exports = sellResource;