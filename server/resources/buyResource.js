'use strict';

/**
 * Buy resource
 */

var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var admin = require('firebase-admin');
var requestCharge = require('../requests/requestCharge');
var variables = require('../env/variables');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

/**
 * Buy resource
 */
var buyResource = {
    buyRequestQueue: require('../requests/buyRequestQueue'),

    /**
     * buy request router
     */
    router: function () {
        var r = express.Router();
        var self = this;
        r.route('/buy')
            /**
             * Adds a new buyrequest to the request queue.
             */
            .post(function (req, res) {

                //firebase authentication idtoken.
                var token = req.query.token;

                var BuyRequest = require('../models/buyRequest');

                //verify idtoken and proceed to add buyrequest
                admin.auth().verifyIdToken(token)
                    .then(function (decodedToken) {

                        var uid = decodedToken.uid;
                        console.log(decodedToken);

                        //create buyrequest object
                        requestCharge.buyCharge(req.body.amount).then(function (charge) {
                            var buyRequest = new BuyRequest(req.body.amount, Date.now(), req.body.to_address, variables.from_address,
                                req.body.phone_number, req.body.money_platform, charge, uid, 'pending');

                            //add buy request to the buyrequest queue.
                            self.buyRequestQueue.enqueue(buyRequest).then(function (request) {
                                res.json(request);
                            }, function (err) {
                                res.status(404).json({
                                    error: err
                                });
                            });
                        });

                    }).catch(function (err) {
                        res.status(404).json({
                            error: err
                        });
                    });
            });
        r.route('/buy/charge').get(function (req, res) {
            var token = req.query.token;
            admin.auth().verifyIdToken(token).then(function (decodedToken) {
                var amount = req.query.amount;
                if (amount == undefined) {
                    res.status(404).json({
                        error: 'amount is undefined'
                    });
                } else {
                    requestCharge.buyCharge(amount).then(function (result) {
                        res.json(result);
                    }, function (err) {
                        res.status(404).json({
                            error: err
                        });
                    });
                }
            }).catch(function (err) {
                res.status(504).json({
                    error: err
                });
            });

        });
        return r;
    }
};
module.exports = buyResource;