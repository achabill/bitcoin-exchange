'use strict';

var variables = require('../env/variables');
var currencyLayer = require('../third-party/currencylayer');

var requestCharge = {
    buyCharge: function (amount) {
        return new Promise(function (resolve, reject) {
            var platform_charge_btc = (variables.buy_transaction_charge_percentage / 100.0) * amount;
            var total_charge_btc = platform_charge_btc + variables.blockchain_fee_btc;
            currencyLayer.convert(variables.bitcoin, variables.francs, total_charge_btc).then(function (result) {
                resolve({
                    xaf: result,
                    btc: total_charge_btc
                });
            });

        });
    },
    sellCharge: function (amount) {
        return new Promise(function (resolve, reject) {
            var platform_charge_btc = (variables.sell_transaction_charge_percentage / 100.0) * amount;
            var total_charge_btc = platform_charge + variables.blockchain_fee_btc;
            currencyLayer.convert(variables.bitcoin, variables.francs, total_charge_btc).then(function (result) {
                resolve({
                    xaf: result,
                    btc: total_charge_btc
                });
            });
        });
    }
};

module.exports = requestCharge;