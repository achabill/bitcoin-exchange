'use strict'


/**
 *  Processes requeusts on the request queue.
 */
var buyRequestProcessor = {
    buyRequestDao: require('../dao/buyRequestDao'),
    futhermarket: require('../third-party/futhermarket'),
    blockchain: require('../third-party/blockchain'),

    process: function (key, request) {
        var self = this;
        self.buyRequestDao.initializeDao();
        return new Promise(function (resolve, reject) {
            //set request status to processing.
            request.status = 'processing';
            self.buyRequestDao.update(key, request).then(function () {
                self.buyRequestDao.getOne(key).then(function (request) {
                    //call futhermarket api to requet payment

                    self.futhermarket.receive(request).then(function (res) {
                        console.log('futhermarket request payment');
                        console.log(res);
                        var paymentId = res.paymentId;
                        //verify payment
                        self.futhermarket.check_receive(paymentId, request.amount).then(function (res) {
                            console.log('furhtermarket check payment');
                            console.log(res);
                            //call block chain
                            //I don't yet know whether this call blocks, executes the transaction immiediatly, or when!
                            self.blockchain.send(request.to_address, request.amount).then(function (response) {
                                console.log('blockchain send coins');
                                console.log(response);
                                request.status = 'complete';
                                self.buyRequestDao.update(key, request).then(function (request) {
                                    resolve(request);
                                }, function (err) {
                                    reject(err);
                                });
                            }, function (err) {
                                reject(err);
                            });
                        }, function (err) {
                            reject(err);
                        })
                    }, function (err) {
                        reject(err);
                    });
                }, function (err) {
                    resolve(err);
                });
            }, function (err) {
                reject(err);
            })
        });
    }
};

module.exports = buyRequestProcessor;