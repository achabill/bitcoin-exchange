'use strict'

/**
 * Queues up the requests for processing
 */


var requestQueue = {
    //buy reqest data access object.
    buyRequestDao: require('../dao/buyRequestDao'),

    //request processor
    requestProcessor: require('./buyRequestProcessor'),

    enqueue: function (request) {
        var self = this;
        //initialize the dao
        self.buyRequestDao.initializeDao();
        return new Promise(function (resolve, reject) {
            //environment variables
            var variables = require('../env/variables')

            //validate min amount on transaction
            if (request.amount < variables.min_amount) {
                reject("Minimum amount for transaction is " + variables.min_amount);
            }

            //save new transaction
            self.buyRequestDao.add(request).then(function (request) {
                var key = request.key;
                self.buyRequestDao.getOne(request.key).then(function (request) {
                    if (variables.auto_process) {
                        //process request and get a completed request
                        self.requestProcessor.process(key, request).then(function (request) {
                            resolve(request);
                        }, function (error) {
                            reject(error);
                        });
                    } else {
                        resolve(request);
                    }
                }, function (error) {
                    reject(error);
                });
            }, function (error) {
                reject(error);
            });
        });
    }
};

module.exports = requestQueue;