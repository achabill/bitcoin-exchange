'use strict'


/**
 *  Processes requeusts on the request queue.
 */
var sellRequestProcessor = {
    sellRequestDao: require('../dao/sellRequestDao'),
    futhermarket: require('../third-party/futhermarket'),
    blockchain: require('../third-party/blockchain'),

    process: function (key, request) {
        var self = this;
        self.sellRequestDao.initializeDao();
        return new Promise(function (resolve, reject) {
            //first receie charge
            self.futhermarket.receive(request, true).then(function (res) {
                var paymentId = res.paymentId;
                //verify payment
                self.futhermarket.check_receive(paymentId).then(function (res) {
                    //call block chain
                    self.blockchain.receive(key).then(function (response) {
                        request.status = 'processing';
                        self.sellRequestDao.update(key, request).then(function (request) {
                            resolve(request);
                        }, function (err) {
                            reject(err);
                        });
                    }, function (err) {
                        reject(err);
                    });
                }, function (err) {
                    reject(err);
                })
            }, function (err) {
                reject(err);
            });
        });
    }
};

module.exports = sellRequestProcessor;