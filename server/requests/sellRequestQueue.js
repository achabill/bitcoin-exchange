'use strict'

/**
 * Queues up the requests for processing
 */


var requestQueue = {
    //buy reqest data access object.
    sellRequestDao: require('../dao/sellRequestDao'),

    //request processor
    requestProcessor: require('./sellRequestProcessor'),

    enqueue: function (request) {
        var self = this;
        //initialize the dao
        self.sellRequestDao.initializeDao();
        return new Promise(function (resolve, reject) {
            //environment variables
            var variables = require('../env/variables')

            //validate min amount on transaction
            if (variables.min_amount < request.amount) {
                reject("Minimum amount for transaction is " + variables.min_amount);
            }

            //save new transaction
            self.sellRequestDao.add(request).then(function (request) {
                var key = request.key;
                self.sellRequestDao.getOne(request.key).then(function (request) {
                    if (variables.auto_process) {
                        //process request and get a completed request
                        self.requestProcessor.process(key, request).then(function (request) {
                            resolve(request);
                        }, function (error) {
                            reject(error);
                        });
                    } else {
                        resolve(request);
                    }
                }, function (error) {
                    reject(error);
                });
            }, function (error) {
                reject(error);
            });
        });
    }
};

module.exports = requestQueue;