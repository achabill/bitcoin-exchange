# CryptoExchange

A platform to exchange cryoto currencies with caash.

## Server
The server is built on node.js

### Running the server
Install dependencies  
`server\$ npm install`  
Run the server  
`server\$ node server.js`

## Client
The client is built on **Ionic v2.+**

### The dependencies for Running the Client
Install the following dependencies. The current version of the dependencies at which the system is being developed on are stated there..
 - Nodejs v6.10.1
 - NPM v3.10.10
 - Ionic v3.18.0 [i think comes with cordova]

### Installing the dependencies
 - Nodejs  
    Get node from their official site and it comes with npm

 - Ionic  
    comand line and type "npm install -g ionic cordova"

### Running the Client
Navigate to the root directory of the client and run   
`client\$ ionic serve`


## Database
Firebase admin SDK for node. https://firebase.google.com/docs/database/admin/

## Who are we?
 - server: [Jafar](https://github.com/achabill)
 - client: [lewizho99](https://github.com/lewizho99)